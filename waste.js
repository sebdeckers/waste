module.exports = function waste (time) {
  return new Promise(function (resolve) {
    setTimeout(resolve, time)
  })
}
