import test from 'ava'
import waste from '.'

test('Sleep for a second', async (t) => {
  const before = Date.now() / 1e3
  await waste(1000)
  const after = Date.now() / 1e3
  t.is(1, Math.round(after - before))
})
