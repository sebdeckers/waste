# waste ⏲

Tiny utility to sleep for a given amount of time.

## Installation

```
npm install --save waste
```

## Usage

```js
import waste from 'waste'

const ONE_HOUR = 60 * 60 * 1000

waste(ONE_HOUR)
  .then(() => {
    // 1 hour later...
  })
```
